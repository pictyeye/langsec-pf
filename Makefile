check: tools/hammer.tool tools/parsifal.tool tools/nom.tool
	@./run-test.sh magic nom
	@./run-test.sh magic parsifal
	@./run-test.sh magic hammer
	@./run-test.sh int-pair parsifal
	@./run-test.sh int-pair hammer
	@./run-test.sh varlen-str parsifal
	@./run-test.sh varlen-str hammer
	@./run-test.sh int-list parsifal
	@./run-test.sh int-list hammer

tools/%.tool: tools/% tools/%/default-commit tools/%/docker/Dockerfile tools/%/source-file tools/%/depends
	./build-tool.sh $<
	touch $@

-include tools/*/depends

clean:
	rm -f tools/*.tool

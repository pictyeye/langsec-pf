# Scripts

Maintenant, si tu veux construire l'image de base pour un outil, il
faut faire

    make tools/<TOOL>.tool

qui appelle le script ./build-tool.sh

Pour lancer un test, il faut faire

    ./run-test.sh <TEST> <TOOL>

Par exemple, on peut faire

    ./run-test.sh magic parsifal


# Options pour lancer les scripts

Avec parsifal, tu peux changer le comportement du programme pour qu'il
affiche le contenu :

    ./run-test.sh -b print magic parsifal

Il faudrait ajouter ce comportement aussi dans hammer. Je le ferai
prochainement.


# Nouveau test

Pour créer un nouveau test, tu peux utiliser le script

    ./new-test.sh <TEST>

qui crée le répertoire dans tests/ ainsi que des sous-répertoires
good/ et bad/ pour l'instant vide.


Il faut ensuite créer des fichiers valides (dans good/) et invalides
(dans bad/).


# Implémentation de la spécification avec un outil

    ./new-implem <TEST> <TOOL>

qui crée le sous-répertoire tools/<TOOL> avec le fichier source de
référence.


# Exemple complet avec varlen-string

Voyons comment j'ai créé le test varlen-string (une chaîne de
caractères préfixée par sa longueur sur un octet).

Commençons par créer les répertoires

    ./new-test varlen-string

Ajoutons des tests valides

    printf "\x00" > tests/varlen-str/good/empty
    printf "\x01A" > tests/varlen-str/good/one-char
    printf "\x02AB" > tests/varlen-str/good/two-chars
    printf "\x06ABCDEF" > tests/varlen-str/good/many-chars
    printf "\xFF%255s" "" > tests/varlen-str/good/255-chars

Et des tests invalides

    printf "" > tests/varlen-str/bad/really-empty
    printf "\x04ABCDtrailing" > tests/varlen-str/bad/trailing
    printf "\x04AB" > tests/varlen-str/bad/truncated

Commençons l'implémentation en parsifal

    ./new-implem.sh varlen-str parsifal

Cela crée le fichier tests/varlen-str/tools/parsifal/target.ml qu'on
va éditer pour implémenter la spécification :

    open BasePTypes
    alias target = string[uint8]

Et normalement, c'est bon !

    ./run-test.sh varlen-string parsifal

open Parsifal

type 'a result = OK of 'a | Error of string

type behaviour = JustParse | Print


let parse_object () =
  try
    let i = string_input_of_filename "target.bin" in
    let target_object = exact_parse Target.parse_target i in
    OK target_object
  with
  | ParsingException (e, h) -> Error (string_of_exception e h)
  | e -> Error (Printexc.to_string e)

let exit_code = function
  | OK _ -> 0
  | Error _ -> 1


let _ =
  let behaviour = match List.tl (Array.to_list Sys.argv) with
    | [] | ["parse"] -> JustParse
    | ["print"] -> Print
    | _ ->
       prerr_endline "Invalid arguments... going back to JustParse";
       JustParse
  in
  match behaviour with
  | JustParse ->
     let result = parse_object () in
     exit (exit_code result)
  | Print ->
     let result = parse_object () in
     begin
       match result with
       | OK x -> print_endline (print_value (Target.value_of_target x))
       | Error e -> prerr_endline e
     end;
     exit (exit_code result)

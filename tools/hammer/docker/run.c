#include <stdio.h>
#include <stdlib.h>

#include "target.c"


int main (int argc, char* argv[]) {
  uint8_t input[1024];
  size_t inputsize;

  FILE *fp = fopen ( "target.bin" , "rb" );
  fseek( fp , 0L , SEEK_END);
  long lSize = ftell( fp );
  rewind( fp );

  const uint8_t* buffer = calloc( 1, lSize+1 );
  if( !buffer ) fclose(fp),fputs("memory alloc fails",stderr),exit(1);

  if( lSize!=fread((void*)buffer , 1, lSize , fp) )
    fclose(fp),free(buffer),fputs("entire read fails",stderr),exit(1);

  HParser *parser = build_parser();

  HParseResult *result = h_parse(parser, buffer, lSize);
  if(result) {
    return 0;
  } else {
    return 1;
  }

  fclose(fp);
  free(buffer);
}

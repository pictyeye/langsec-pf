use std::io::prelude::*;
use std::fs::File;

mod target;

fn main() -> std::io::Result<()> {
    let mut file = File::open("target.bin")?;
    let mut contents = vec![];
    file.read_to_end(&mut contents)?;
    if target::parse_target(&contents) == Ok((&b""[..], &contents)){
        println!("OK\n");
    } else{
        println!("nay\n");
        std::process::exit(1);
    }
    Ok(())
}

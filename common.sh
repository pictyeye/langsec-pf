DOCKER_OPTIONS="-q"
if [ -n "$VERBOSE" ]; then DOCKER_OPTIONS=; fi

error() {
    echo "Error: $1" >&2
    echo "$USAGE"
    [ -n "$VERBOSE" ] && echo -n "$LOG"
    exit 1
}

log_missing_file() {
    LOG="${LOG}Could not find directory \"$1\"
"
}

log_missing_dir() {
    LOG="${LOG}Could not find directory \"$1\"
"
}

check_tool_dir() {
    if [ ! -d "$1" ]; then log_missing_dir "$1"; return 1; fi
    if [ ! -f "$1/default-commit" ]; then log_missing_file "$1/default-commit"; return 1; fi
    if [ ! -f "$1/source-file" ]; then log_missing_file "$1/source-file"; return 1; fi
    if [ ! -d "$1/docker" ]; then log_missing_dir "$1/docker"; return 1; fi
    return 0
}

define_tool_vars() {
    if check_tool_dir "$1"; then
        TOOL_DIR="$1"
    elif check_tool_dir "tools/$1"; then
        TOOL_DIR="tools/$1"
    else
        error "$1 does not seem to be a valid tool."
    fi
    LOG=""
    TOOL_NAME="$(basename "$1")"
    if [ -n "$2" ]; then
        TOOL_COMMIT="$2"
    else
        TOOL_COMMIT="$(cat "$TOOL_DIR/default-commit")"
    fi
    TOOL_TAG="langsec-pf--$TOOL_NAME:$TOOL_COMMIT"
    TOOL_SOURCE_FILENAME="$(cat "$TOOL_DIR/source-file")"
    if [ -f "$TOOL_DIR/container-dir" ]; then
        TOOL_CONTAINER_DIR="$(cat "$TOOL_DIR/container-dir")"
    else
        TOOL_CONTAINER_DIR="/root/test"
    fi
    if [ -f "$TOOL_DIR/container-source-dir" ]; then
        TOOL_CONTAINER_SOURCE_DIR="$(cat "$TOOL_DIR/container-source-dir")"
    else
        TOOL_CONTAINER_SOURCE_DIR=""
    fi
}

check_test_dir() {
    if [ ! -d "$1" ]; then log_missing_dir "$1" return 1; fi
    if [ ! -d "$1/good" ]; then log_missing_dir "$1/good"; return 1; fi
    if [ ! -d "$1/bad" ]; then log_missing_dir "$1/bad"; return 1; fi
    if [ -n "$TOOL_NAME" ] && [ ! -d "$1/tools/$TOOL_NAME" ]; then
        log_missing_dir "$1/tools/$TOOL_NAME";
        return 1;
    fi
    return 0
}

define_test_vars() {
    if check_test_dir "$1"; then
        TEST_DIR="$1"
    elif check_test_dir "tests/$1"; then
        TEST_DIR="tests/$1"
    else
        error "$1 does not seem to be a valid test (maybe it is not implemented for $TOOL_NAME)."
    fi
    LOG=""
    TEST_NAME="$(basename "$1")"
}

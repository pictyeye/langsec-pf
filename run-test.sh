#!/bin/bash

set -e

BASEDIR=$(dirname "$0")
cd "$BASEDIR"

USAGE="Usage: $0 [-b parse|print] [-v|-q] [-c|-C] TEST_NAME TOOL [COMMIT]"
BEHAVIOUR=parse

while getopts "b:vqcCh" arg; do
    case $arg in
        h)
            echo "$USAGE"
            exit 0
            ;;
        b)
            case "$OPTARG" in
                parse|print)
                    BEHAVIOUR="$OPTARG"
                    ;;
                *)
                    echo "Invalid behaviour ($OPTARG)"
                    echo "$USAGE"
                    exit 1
                    ;;
            esac
            ;;
        v) VERBOSE=1;;
        q) VERBOSE=;;
        c) DONOTCLEAN=1;;
        C) DONOTCLEAN=;;
        *)
             echo "Invalid option ($arg)"
             echo "$USAGE"
             exit 1
             ;;
  esac
done

shift $((OPTIND-1))


. common.sh


define_tool_vars "$2" "$3"
define_test_vars "$1"

echo "Running test $TEST_NAME for $TOOL_NAME..."

echo "  Building the container..."

TMPDIR="$(mktemp -d /tmp/tmpXXXXXXX)"
cp -- "$TEST_DIR/tools/$TOOL_NAME/$TOOL_SOURCE_FILENAME" "$TMPDIR/"
cat > "$TMPDIR/Dockerfile" <<EOF
FROM $TOOL_TAG
COPY $TOOL_SOURCE_FILENAME ${TOOL_CONTAINER_SOURCE_DIR}$TOOL_SOURCE_FILENAME
RUN make
ENTRYPOINT ["./run"]
EOF
IMAGE=$(docker build -q "$TMPDIR")
if [ -z "$DONOTCLEAN" ]; then
    rm -rf "$TMPDIR"
else
    echo "Keeping temporary docker directory \"$TMPDIR\""
fi

echo "  Testing good test cases..."
for SAMPLE in "$TEST_DIR/good/"*; do
    echo "    $(basename "$SAMPLE")..."
    if ! docker run -ti --rm -v "$PWD/$SAMPLE":"$TOOL_CONTAINER_DIR"/target.bin "$IMAGE" "$BEHAVIOUR"; then
        error "$TOOL did not handle correctly sample $(basename "$SAMPLE") for test $TEST_NAME."
    fi
done

echo "  Testing bad test cases..."
for SAMPLE in "$TEST_DIR/bad/"*; do
    echo "    $(basename "$SAMPLE")..."
    if docker run -ti --rm -v "$PWD/$SAMPLE":"$TOOL_CONTAINER_DIR"/target.bin "$IMAGE" "$BEHAVIOUR"; then
        error "$TOOL did not handle correctly sample $(basename "$SAMPLE") for test $TEST_NAME."
    fi
done

if [ -z "$DONOTCLEAN" ]; then
    if [ -n "$VERBOSE" ]; then
        docker image rm "$IMAGE"
    else
        docker image rm "$IMAGE" &> /dev/null
    fi
else
    echo "Keeping temporary docker image \"$IMAGE\""
fi

echo "Running test $TEST_NAME for $TOOL_NAME: OK"

#!/bin/bash

set -e

BASEDIR=$(dirname "$0")
cd "$BASEDIR"

USAGE="Usage: $0 TOOL [COMMIT]"

. common.sh


define_tool_vars "$1" "$2"

echo "Building and testing base image for $TOOL_NAME..."

if ! docker build $DOCKER_OPTIONS -t "$TOOL_TAG" --build-arg COMMIT="$TOOL_COMMIT" "$TOOL_DIR/docker"; then
    error "Docker for $TOOL_NAME could not be built."
fi
if ! docker run -ti --rm "$TOOL_TAG"; then
    error "Docker for $TOOL_NAME does not seem to run correctly."
fi
echo "Building and testing base image for $TOOL: OK"

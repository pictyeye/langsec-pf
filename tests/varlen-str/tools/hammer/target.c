#include<hammer/hammer.h>

HParser *build_parser(){
  HParser *length = h_uint8();
  HParser *len_data= h_length_value(length, h_uint8());
  HParser *parser = h_sequence(len_data, h_end_p(), NULL);
  return parser;
}

#!/bin/bash

set -e

BASEDIR=$(dirname "$0")
cd "$BASEDIR"

USAGE="Usage: $0 TEST_NAME"

. common.sh

TEST_NAME="$1"

if [ -z "$TEST_NAME" ]; then
    error "Please provide a test name."
fi

if [ -n "$(echo "$TEST_NAME" | tr -d -- '-a-zA-Z0-9_')" ]; then
    error "TEST_NAME should only contain letters, digits, dashes and underscores."
fi

if check_test_dir "tests/$TEST_NAME"; then
    error "Test $TEST_NAME seems to already exist"
fi

mkdir -p tests/"$TEST_NAME"/good
mkdir -p tests/"$TEST_NAME"/bad
mkdir -p tests/"$TEST_NAME"/tools


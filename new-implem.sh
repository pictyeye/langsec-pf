#!/bin/bash

set -e

BASEDIR=$(dirname "$0")
cd "$BASEDIR"

USAGE="Usage: $0 TEST_NAME TOOL_NAME"

. common.sh

define_test_vars "$1"
define_tool_vars "$2"

mkdir -p "$TEST_DIR/tools/$TOOL_NAME"
cp "$TOOL_DIR/docker/$TOOL_SOURCE_FILENAME" "$TEST_DIR/tools/$TOOL_NAME/$TOOL_SOURCE_FILENAME"

